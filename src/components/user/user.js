import React, {Component} from 'react';

class User extends Component {
  state = {
    user: undefined,
  };
  componentDidMount() {
    fetch('https://randomuser.me/api/')
        .then(response => response.json())
        .then(data => {
          this.setState({user: data.results[0]});
        });
  }
  handleClickUserName = () => {
    this.targetUserName.style.textDecoration = 'line-through';
  };
  handleClickAge= () => {
    this.targetAge.style.textDecoration = 'line-through';
  };
  handleClickEmail= () => {
    this.targetEmail.style.textDecoration = 'line-through';
  };
  handleClickTel= () => {
    this.targetTel.style.textDecoration = 'line-through';
  };
  render() {
    const {user} = this.state;
    if (user == undefined) {
      return '';
    }
    return (
      <div className="user">
        <img className="photo" src={user.picture.large} />
        <p className="name">
          {user.name.first} {user.name.last}
        </p>
        <div className="infos">
          <div className="flex">
            <p className="label" onClick={this.handleClickUserName}>
              Nom d'utilisateur: </p>
            <p className="target" ref={targetUserName => (this.targetUserName = targetUserName)}>
              {user.login.username}
            </p>
          </div>
          <div className="flex">
            <p className="label" onClick={this.handleClickAge}>
              Age: </p>
            <p className="target" ref={targetAge => (this.targetAge = targetAge)}>
              {user.dob.age}
            </p>
          </div>
          <div className="flex">
            <p className="label" onClick={this.handleClickEmail}>
              Email: </p>
            <p className="target" ref={targetEmail => (this.targetEmail = targetEmail)}>
              {user.email}
            </p>
          </div>
          <div className="flex">
            <p className="label" onClick={this.handleClickTel}>
              Telephone: </p>
            <p className="target" ref={targetTel => (this.targetTel = targetTel)}>
              {user.phone}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default User;
