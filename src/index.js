import React from 'react';
import ReactDOM from 'react-dom';
import './components/user/style.scss';
import User from './components/user/user';

const app = (
  <div>
    <User />
  </div>
);

const ReactRoot = document.getElementById('react-root');
ReactDOM.render(app, ReactRoot);
